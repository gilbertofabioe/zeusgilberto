import '../App.css';
import {useState, useEffect} from 'react'
import {BsTrash, BsBookmarkCheck, BsBookmarkCheckFill} from 'react-icons/bs'
import {FiEdit3} from 'react-icons/fi' 


//const API = "http://localhost:5000";
const API = "http://localhost:5001";
const DataBase = "/gastos";

const TodoList = () => {

    const [title, setTitle] = useState('');
    const [valor, setValor] = useState();
    const [banho, setBanho] = useState(false);
    const [vet, setVet] = useState(false);
    const [comida, setComida] = useState(false);
    const [todosGlobal, setTodosGlobal] = useState([])
    const [todos, setTodos] = useState([]);
    const [loading, setLoaing] = useState(false);
    const [editing, setEditing] = useState(null);
    const [deleting, setDeleting] = useState(null);
    const [editValue, setEditValue] = useState({});
    const [refreshing, setRefresh] = useState(false);
    const [year, setYear] = useState(2022)
    const [month, setMonth] = useState("00")
    //const [total, setTotal] = useState()
    //Carregar gastos no pageload
    useEffect(()=>{
      const loadData = async() =>{
        setLoaing(true);
        const res = await fetch(API + "/gastos")
        .then((res)=> res.json())
        .then((data)=>data)
        .catch((err)=>console.log(err))
        setLoaing(false);
        setTodosGlobal(res);
        setTodos(res.sort((a,b)=> new Date(b.date).getTime() - new Date(a.date).getTime()).filter(todo => todo.date.split('-')[0]===`${year}`))
        setRefresh(false);
        setBanho(false)
        setComida(false)
        setVet(false)
        setYear(2022)
        setMonth("00")
      };
      loadData();
    },[refreshing])

    const getItems = async() =>{
      const res = await fetch(API + "/gastos")
      .then((res)=> res.json())
      .then((data)=>data)
      .catch((err)=>console.log(err))
      setEditValue({})
      handleCancel()
      setTodosGlobal(res);
      setTodos(res.sort((a,b)=> new Date(b.date).getTime() - new Date(a.date).getTime()).filter(todo => todo.date.split('-')[0]===`${year}`))
      
    }

    const handleFilter = (mes, ano, b, v, c)=>{

      if(b===true || v ===true || c===true){
        if(month === "00"){
          setTodos(todosGlobal.filter(todo => todo.date.split('-')[0]===`${year}`).filter(todo => todo.banho=== b || !b).filter(todo => todo.vet=== v || !v).filter(todo => todo.comida=== c || !c))
        }else{
          setTodos(todosGlobal.filter(todo => todo.date.split('-')[1]===`${month}`).filter(todo => todo.date.split('-')[0]===`${year}`).filter(todo => todo.banho===b || !b).filter(todo => todo.vet===v || !v).filter(todo => todo.comida===c || !c))
        }
      }else{
        if(month === "00"){
          setTodos(todosGlobal.filter(todo => todo.date.split('-')[0]===`${year}`))
        }else{
          setTodos(todosGlobal.filter(todo => todo.date.split('-')[1]===`${month}`).filter(todo => todo.date.split('-')[0]===`${year}`))
        }
      }
      
      /*if(mes === "00"){
        setTodos(todosGlobal.filter(todo => todo.date.split('-')[0]==`${year}`))
      }else{
        setTodos(todosGlobal.filter(todo => todo.date.split('-')[1]===`${mes}`).filter(todo => todo.date.split('-')[0]==`${year}`))
      }*/


      /*.filter(todo => todo.banho===b).filter(todo => todo.vet===v).filter(todo => todo.comida===c)
        .filter(todo => todo.vet===v)
        .filter(todo => todo.comida===c)*/
    }

    
    const handleTotal =  ()=>{
      const total =  todos.reduce((res,cur)=> res+cur.valorFormat,0).toFixed(2)
      return total
       //setTotal(todo.reduce((res,cur)=> res+cur.valorFormat,0).toFixed(2))
    }

    const handleDate = (data) =>{
      const [ano,mes,dia] = data.split('-')
  
      return `${dia}/${mes}/${ano}`
    }
  
    const handleDelete = async (id)=>{
  
      await fetch(API + DataBase + "/"+id,{
        method: "DELETE"
      }).then(alert(`Item excluído com sucesso!`)).then(getItems)
      //.then(setTodosGlobal((prevState) => prevState.filter((todo)=>todo._id!==id)))
      //.then(handleCancel()).then(setRefresh(true)); 
  
    }
  
    const handleEdit = async (e)=>{
      e.preventDefault();
      const todo = editValue;
  
      if(valor){
        const valorFormat = parseFloat(valor).toFixed(2);
        todo.valorFormat = valorFormat
      }else{
        const valorFormat = parseFloat(todo.valorFormat).toFixed(2);
        todo.valorFormat = valorFormat
      }
      if(title!==""){
        todo.title = title
      }
  
      //enviar para DB
      const data = await fetch(API + DataBase + "/"+todo._id,{
        method: "PATCH",
        body:JSON.stringify(todo),
        headers: {
          "Content-Type": "application/json",
        },
      }).then(getItems)//.then(setRefresh(true)).then(setEditValue({})).then(handleCancel); 
    
    };
  
    const handleDeleting = (todo)=>{
      
      if(deleting===null){
        setDeleting(todo)
        setEditing(null)
      }else{
        handleCancel()
      }
      
    }
  
    const handleEditing = (todo)=>{
      
      if(editing===null){
        setEditing(todo)
        setDeleting(null)
      }else{
        handleCancel()
      }
      
    }
    
    const handleCancel = ()=>{
  
      setEditing(null)
      setDeleting(null)
      setTitle("")
      setValor()
    }
  
    if(loading){
      return <p>Carregando...</p>
    }
    return(

        <div className='list-todo'>
        <h2>Lista de gastos:</h2>
        <div className='filter-todo'>
          <select className="month-picking" onChange={(e)=>{setMonth(e.target.value)}}>
            <option value="00">--Filtrar por mês--</option>
            <option value="01">Janeiro</option>
            <option value="02">Fevereiro</option>
            <option value="03">Março</option>
            <option value="04">Abril</option>
            <option value="05">Maio</option>
            <option value="06">Junho</option>
            <option value="07">Julho</option>
            <option value="08">Agosto</option>
            <option value="09">Setembro</option>
            <option value="10">Outubro</option>
            <option value="11">Novembro</option>
            <option value="12">Dezembro</option>
          </select>
          <input className='year-picking' 
          type='number' min='1900' max= '2099' 
          step='1' defaultValue='2022'
          onChange={(e)=>{setYear(e.target.value)}}/>
          <div className='actions'>
            <span title='Banho'>Banho: 
              {!banho ? <BsBookmarkCheck onClick={()=>setBanho(!banho)}/>:<BsBookmarkCheckFill onClick={()=>setBanho(!banho)}/>}
            </span>
            <span title='Vet'> Vet: 
              {!vet ? <BsBookmarkCheck onClick={()=>setVet(!vet)}/>:<BsBookmarkCheckFill onClick={()=>setVet(!vet)}/>}
            </span>
            <span title='Comida'> Comida: 
              {!comida ? <BsBookmarkCheck onClick={()=>setComida(!comida)}/>:<BsBookmarkCheckFill onClick={()=>setComida(!comida)}/>}
            </span> 
          </div>

          <button onClick={()=>handleFilter(month, year, banho, vet, comida)} >Filtrar</button>
        </div>
        
        {todos.length ===0 && <p>Não há tarefas</p>}
        {todos.map((todo)=>(
          <div className='todo' key={todo._id}>
            <h3>{todo.title}</h3>
            <p>Data: {handleDate(todo.date)}</p>
            <p>R$: {todo.valorFormat}</p>
            <div className='actions'>
              <span title='Banho'>Banho: 
                {!todo.banho ? <BsBookmarkCheck />:<BsBookmarkCheckFill />}
              </span>
              <span title='Vet'> Vet: 
                {!todo.vet ? <BsBookmarkCheck />:<BsBookmarkCheckFill />}
              </span>
              <span title='Comida'> Comida: 
                {!todo.comida ? <BsBookmarkCheck />:<BsBookmarkCheckFill />}
              </span> 
              <br/>
              <br/>
              <BsTrash onClick={()=>handleDeleting(todo)}/>
              <FiEdit3 onClick={()=>handleEditing(todo)}/>
              <div className='editing'>
                {editing === todo ? (
                <div className='form'>
                  <form onSubmit={handleEdit}>
                    <div className='form-control'>
                      <label htmlFor='title'>Gasto:</label>
                      <input 
                        type="text" 
                        name="title" 
                        onChange={(e) => setTitle(e.target.value)}
                        value={title || todo.title}
                      />
                    </div>
                    <div className='form-control'>
                      <label htmlFor='valor'>Valor do gasto:</label>
                      <input 
                        type="number" 
                        name="valor" 
                        onChange={(e) => setValor(e.target.value)}
                        value={valor || todo.valorFormat}
                      />
                    </div>
                    <button onClick={()=>{
                      setEditValue(editing)
                      console.log(editValue)
                    }} >Salvar</button>
                    <button onClick={handleCancel} >Cancelar</button>
                  </form>
              </div>
              ): <></> }
              <div>
              {deleting === todo ? (
                <div className='delete-button'>
                  <button onClick={()=>handleDelete(todo._id)} >Deletar</button>
                  <button onClick={handleCancel} >Cancelar</button>
                </div>
              ): <></> }
              </div>
              </div>
            </div>
          </div>
        ))}
        <div className='total-value-container'>
          <h2 className='total-value'>
            O seu gasto total foi: {`R$: ${handleTotal()}`}
            </h2>
        </div>
      </div>
        
    )

}

export default TodoList