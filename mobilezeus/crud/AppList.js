import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import AppItem from './AppItem';
import Database from './Database'
//import AsyncStorage from '@react-native-async-storage/async-storage';
export default function AppList({route, navigation}) {
	
  const [items, setItems]= useState([])

  useEffect(()=>{
    //AsyncStorage.clear()
    Database.getItems().then(items => setItems(items))
  }, [route]);
  
  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <Text style={styles.title}>Lista de Gastos</Text>
      <ScrollView 
        style={styles.scrollContainer}
        contentContainerStyle={styles.itemsContainer}>
        { items.map(item => {
          return <AppItem key={item._id} id={item._id} item={`R$: ${item.valorFormat}  de ${item.title}, no dia  ${item.date}`} navigation={navigation}/>
        }) }
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#30c9cf',
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 50,
    marginBottom: 20
  },
  scrollContainer: {
    flex: 1,
    width: '90%'
  },
  itemsContainer: {
    marginTop: 10,
    padding: 20,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignItems: 'stretch',
    backgroundColor: '#fff'
  },
});