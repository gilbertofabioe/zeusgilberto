import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import AppItem from './AppItem';
import Database from './Database'

export default function AppList({route, navigation}) {
	
  const [items, setItems]= useState([])
  const [orderedItems, setOrderedItems] = useState([])

  useEffect(()=>{
    Database.getItems().then(items => setItems(items))
    sorting(items)
  }, [route]);

  const sorting = (list) =>{
    return list.sort((a,b)=> new Date(b.date).getTime() - new Date(a.date).getTime())
  }

  const formatingDate = (data) =>{
    const [ano, mes, dia] = data.split('-')
    return `${dia}/${mes}/${ano}`
  }

  const handleTotal = (list)=>{
    return list.reduce((res,cur)=> res+cur.valorFormat,0).toFixed(2)
 }
  
  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <Text style={styles.title}>Total de gastos: R$:{handleTotal(sorting(items))}</Text>
      <ScrollView 
        style={styles.scrollContainer}
        contentContainerStyle={styles.itemsContainer}>
        { sorting(items).map(item => {
          return <AppItem key={item._id} id={item._id} item={`R$: ${item.valorFormat.toFixed(2)}  de ${item.title}, no dia  ${formatingDate(item.date)}`} navigation={navigation}/>
        }) }
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#30c9cf',
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 50,
    marginBottom: 20
  },
  scrollContainer: {
    flex: 1,
    width: '90%'
  },
  itemsContainer: {
    marginTop: 10,
    padding: 20,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignItems: 'stretch',
    backgroundColor: '#fff'
  },
});