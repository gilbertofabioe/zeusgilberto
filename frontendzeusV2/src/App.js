import MainComponent from './components/maincomponent';
import './App.css';
import React, { useState, useEffect } from 'react'
import { BsTrash, BsBookmarkCheck, BsBookmarkCheckFill } from 'react-icons/bs'
import { FiEdit3 } from 'react-icons/fi'
import TodoList from './components/TodoList';
import TodoForm from './components/TodoForm';

//const API = "http://localhost:5000";
const API = "http://localhost:5001";
const DataBase = "/gastos";

function App() {

  const [refresh, setRefresh] = useState(false)
  const [modalForm, setModalForm] = useState(false)
  
  useEffect(()=>{
    const loadData = async() =>{
      setModalForm(false)
    };
    loadData();
  },[])

  return (

    <div>
      <MainComponent />
      <div className="App">
      {modalForm === true ? <TodoForm refresh={refresh} setRefresh={setRefresh} modalForm={modalForm} setModalForm={setModalForm}/>
      : <></>}

        
      <TodoList refresh={refresh} setRefresh={setRefresh} modalForm={modalForm} setModalForm={setModalForm}/>
      </div>


    </div>

  );
}

export default App;
